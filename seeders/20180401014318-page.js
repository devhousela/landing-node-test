'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    
      return queryInterface.bulkInsert('pages', [{
        name: 'pagina 1',
        description: "nada",
        status: 1,
        position: 0
      }], {});
   
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('pages', null, {});
  }
};
