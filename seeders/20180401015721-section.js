'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('sections', [{
      name: 'Seccion 1',
      description: "nada",
      content: "contenido aun no blob",
      status: 1,
      page_id: 1,
      position: 0
    }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('sections', null, {});
  }
};
