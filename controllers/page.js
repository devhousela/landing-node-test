'use strict'

var models = require('../models'); // loads index.js
var Page = models.Page;       // the model keyed by its name


function getPages(req, res) {

    Page.findAll({ 
        where: { status: 1 },
        include: [ { 
            model: models.Section,
            where: { status: 1 },
            required: false
        } ] 
    }).then(pages => {
        if (pages == "") {
            return res.status(404).send({ message: 'there are not active pages' });
        }
        return res.status(200).send({ pages });
    }).catch(function (err) {
        return res.status(500).send({ message: 'Error getting the pages' });
    });
}


function getPage(req, res) {
    
    var id = req.params.id;

        Page.findOne({ 
            where: { 
                id: id
            },
            include: [ { 
                model: models.Section,
                where: { status: 1 },
                required: false
            } ] 
        }).then(page => {
            if (!page) {
                return res.status(404).send({ message: 'this page does not exist' });
            }
            return res.status(200).send({ page });
        }).catch(function (err) {
            return res.status(500).send({ message: 'Error getting the page' });
        });
    }


    function savePage(req, res){
        var newPage = req.body;

        Page.create(newPage).then(page => {
            if (!page) {
                return res.status(404).send({ message: 'thes page was not created' });
            }
            return res.status(200).send({ page });
        }).catch(function (err) {
            return res.status(500).send({ message: 'Error saving the page' +err });
        });
    }

    function updatePage(req, res){
        var id = req.params.id;
        var updatePage = req.body;

        Page.update(updatePage,
            {
                where : { id: id }
            }
        ).then(page => {
            if (!page) {
                return res.status(404).send({ message: 'thes page was not updated' });
            }
            return res.status(200).send({ updatePage });
        }).catch(function (err) {
            return res.status(500).send({ message: 'Error updating the page ' +err });
        });
    }

    function deletePage(req, res){
        var id = req.params.id;
        Page.destroy(
            {
                where : { id: id }
            }
        ).then(page => {
            if (!page) {
                return res.status(404).send({ message: 'the page was not deleted' });
            }
            return res.status(200).send({ page });
        }).catch(function (err) {
            return res.status(500).send({ message: 'Error deleting the page ' +err });
        });
    }


module.exports = {
    getPages,
    getPage,
    savePage,
    updatePage,
    deletePage
}