'use strict';


module.exports = (sequelize, DataTypes) => {
  var Section = sequelize.define('Section', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    status: DataTypes.INTEGER,
    position: DataTypes.INTEGER,
    content: DataTypes.BLOB,
    page_id: DataTypes.INTEGER
  }, {});
  Section.associate = function(models) {
    Section.belongsTo(models.Page, {foreignKey: 'page_id'});
  };
  return Section;
};

