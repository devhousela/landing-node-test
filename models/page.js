'use strict';


module.exports = (sequelize, DataTypes) => {
  var Page = sequelize.define('Page', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    status: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: true,
        isInt: true
      }
    },
    position: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: true,
        isInt: true
      }
    }
  }, {});
  Page.associate = function(models) {
    Page.hasMany(models.Section, {foreignKey: 'page_id'});
  };
  return Page;
};

