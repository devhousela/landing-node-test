'use strict'

var app = require('./app');
const Sequelize = require('sequelize');

var port = process.env.port || 3000;



const sequelize = new Sequelize('node_landings', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',
    port: 3306
  });


sequelize
.authenticate()
.then(() => {
  console.log('Connection has been established successfully.');
  app.listen(port, function(){
        console.log('Server nodejs running ');
    });
})
.catch(err => {
  console.error('Unable to connect to the database:', err);
});

