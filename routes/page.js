'use strict'

var express = require('express');
var PageController = require('../controllers/page');
var api = express.Router();

//routes
api.get('/paginas', PageController.getPages );
api.get('/pagina/:id', PageController.getPage );
api.post('/pagina', PageController.savePage );
api.put('/pagina/:id', PageController.updatePage );
api.delete('/pagina/:id', PageController.deletePage );


module.exports = api;